// Instructions: Write a function that takes a string, and returns the character that is most commonly used in the string.

let sentence = "I am usingatest sentence."

const repeatedChar = ((sentence) => {
  
    let alphabet = {}

    sentence.toLowerCase();

    for (let i = 0 ; i < sentence.length ; i++){
        alphabet[sentence[i]] = alphabet[sentence[i]] ? alphabet[sentence[i]]+1 : 1;
    }

    let repeatedChar = Object.keys(alphabet).reduce((a, b) => alphabet[a] > alphabet[b] ? a : b);

    console.log(repeatedChar);

})

repeatedChar(sentence);