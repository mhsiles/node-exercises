// Instructions: Given a string, write a function that will return whether or not that string is a palindrome.

const isPalindrome = ((phrase) => {

    // delete special chars and reverse word
    const charArray = phrase.split('').filter(char => char != ' ');

    return charArray.join() === charArray.reverse().join();

});

console.log(isPalindrome('ligar es ser agil'));