const collection = [ 'binary', 'cinema', 'start', 'orange',  'iceman',  'brainy',  'rats',  'tarts',  'star','starts'];

//Level 1
// Instructions: Create a function that takes in two strings as two parameters and returns a boolean
// that indicates whether or not the first string is an anagram of the second string.
const areAnagrams = (( word1, word2 ) => {

    // sorte letters 
    const sortedWord1 = word1.split('').sort();
    const sortedWord2 = word2.split('').sort();

    // merge chars into string and compare
    return sortedWord1.join() === sortedWord2.join()

});

console.log(areAnagrams('teresas', 'sereta'));

// Level 2
// Given an array of words, determine which of them are anagrams
const collectionAnagrams = (collection) => {

    let anagramCollection = new Map();

    for (let i = 0 ; i < collection.length ; i++){
        const orderedWord = collection[i].split('').sort().join('');
        if(!anagramCollection.has(orderedWord)){
            anagramCollection.set(orderedWord, [collection[i]]);
        }else{
            let keyValue = anagramCollection.get(orderedWord);
            keyValue.push(collection[i]);
            anagramCollection.set(orderedWord, keyValue);
        }
    }

    return [...anagramCollection.values()]; //iterator
};

console.log(collectionAnagrams(collection));

//Level 3
//Based on the same collections, determine if the words are anagrams divided by the following catergories:
// Perfect match: words have same letters and length
// Near match: words have same letters but different length
// Create and array showing the results, where the key value to be compared must be the smalles word
const matchingAnagrams = (collection) => {

    collection.sort((a,b) => a.length - b.length); //sort by element (word) length

    let anagramCollection = new Map(); // create result Map
    
    for (let i = 0 ; i < collection.length ; i++){ // for each element in the collection array
        
        let foundKey = [...anagramCollection.keys()].filter(key => { // iterate over the map keys to find any matching key
            return new Set(collection[i]).size === new Set(collection[i] + key).size; // check if the key is contained in the new word
        });
        if (foundKey.length == 0){ //if no key was found
            let anagramWords = {
                perfect: [],
                near: []
            };
            anagramCollection.set(collection[i], anagramWords); //create a new key
        }else{
            let keyValue = anagramCollection.get(foundKey[0]); // get current key value
            if (foundKey[0].length == collection[i].length){ //if new word has the same length --> perfect match
                keyValue.perfect.push(collection[i]);
            }else{
                keyValue.near.push(collection[i]); //near match
            }
            anagramCollection.set(foundKey[0], keyValue); //set the new value
        }
        
    }
    console.log(anagramCollection);
    return [...anagramCollection];
    
};

const collection = [ 'binary', 'cinema', 'start', 'orange',  'iceman',  'brainy',  'rats',  'tarts',  'star','starts']

console.log(matchingAnagrams(collection));
