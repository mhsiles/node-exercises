// Given a string possibly containing three types of braces ({}, [], ()), write a function that returns a Boolean indicating whether the given string contains a valid nesting of braces.

const string = "let function = ((variable) => {return new var = array[]});"

const validateBraces = string => {

    const openingBraces = ['[', '(', '{'];
    const closingBraces = [']', ')', '}'];

    const charArray = string.split('');

    let bracesStack = [];

    charArray.forEach(element => {
        // Add new opening brace to the stack
        if (openingBraces.includes(element)){
            bracesStack.push(element);
        }else if (closingBraces.includes(element)){
            // Get the closing element
            const closingIndex = closingBraces.indexOf(element);
            const popedIndex = openingBraces.indexOf(bracesStack.pop());
            // Compare if the closing element correspond to its matching opening element previously opened
            if ( closingIndex != popedIndex ) return false;
        }
        console.log(bracesStack);
    });

    return bracesStack.length == 0;
};

console.log(validateBraces(string));