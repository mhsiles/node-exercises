// Instructions: Given an array of objects, sort the objects by population size. Return the entire object.

const countries = [
    {
        name: 'Mexico',
        population: 130
    },
    {
        name: 'USA',
        population: 350
    },
    {
        name: 'Canada',
        population: 40
    },
    {
        name: 'China',
        population: 1450
    },
]

// Send object and attribute to consider for the sort
const orderObjects = (countries, attribute, ascending) => {

    return ascending == 'asc' ? 
        // Sort ascending
        countries.sort((a,b) => a[attribute] > b[attribute] ? 1 : -1) : 
        // Sort descending
        countries.sort((a,b) => a[attribute] > b[attribute] ? -1 : 1);

}

// Send attribute when running the program
console.log(orderObjects(countries, process.env.ATTRIBUTE, process.env.SORT));