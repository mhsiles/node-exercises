# Node Exercises

Different programming challenges solved with NodeJs (Javascript)

## Menu

+ Longest string in Array
+ Most repeated char
+ Anagrams
+ Palindromes
+ Balanced brackets
+ Armstrong numbers
+ Sorting objects
+ Reversed linked list