// Instructions: An Armstrong number is an n-digit number that is equal to the sum of the nthnth powers of its digits.
// Determine if the input number is an Armstrong number. Return either true or false.


const isArmstrong = number => {

    // Separate numbers into 'chars'
    const numberArray = number.toString().split('');
    // get number of digits
    const pow = numberArray.length;
    let sum = 0;

    sum = numberArray.reduce((a,b) => {
        return parseInt(a) + Math.pow(parseInt(b),pow);
    });

    return number === sum;

};

console.log(isArmstrong(153));