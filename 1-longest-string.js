// Instructions: Write a function that accepts an array of strings. Return the longest string.

const names = ['Mauricio', 'Jorge', 'Valdemar', 'Francisco'];

const getLargestName = ((names) => {

    // Iterate for each name and return the longest name
    return names.reduce((a, b) => {
        // compate length of names
        return a.length > b.length ? a : b;
    });

});

console.log(getLargestName(names)); 